//
//  Item.swift
//  ToDoList
//
//  Created by Marcin Kępa on 22/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import Foundation

class Item: Codable {
    var title: String = ""
    var done: Bool = false
}
